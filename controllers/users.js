const { response } = require('express');
const usersData = require('../models/user');

const getUsers = ( req, res = response ) => {
    const data = usersData;

    return res.status(200).json({
        ok: true,
        data
    });
}

module.exports = getUsers;