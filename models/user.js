const usersData = [
	{
		nombre: 'wade',
		apellido: 'reilly',
		email: 'mauris@lacinia.net',
		direccion: '8721 Est, Avenue'
	},
	{
		nombre: 'levi',
		apellido: 'larsen',
		email: 'nullam.suscipit@erategetipsum.edu',
		direccion: '8518 Magna Street'
	},
	{
		nombre: 'jerome',
		apellido: 'schroeder',
		email: 'a.mi.fringilla@diamPellentesque.org',
		direccion: 'Ap #334-3825 Sem, Street'
	},
	{
		nombre: 'zachery',
		apellido: 'ball',
		email: 'suspendisse.commodo.tincidunt@SedmolestieSed.co.uk',
		direccion: 'Ap #812-9209 Nam Rd.'
	},
	{
		nombre: 'maxwell',
		apellido: 'alvarez',
		email: 'rutrum.urna@Nuncpulvinararcu.com',
		direccion: '1503 Morbi Rd.'
	}
];

module.exports = usersData;