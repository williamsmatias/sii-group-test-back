const { Router } = require('express');
const getUsers = require('../controllers/users');

const router = Router();

/**
 * Obtener data de usuarios
 */
router.get('/', getUsers );

module.exports = router;